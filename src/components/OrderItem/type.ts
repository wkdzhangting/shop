import type { OrderItemParams } from '@/pages/Order/type';

export type OrderItemProps = {
  key: number;
  orderInfo: OrderItemParams;
};
