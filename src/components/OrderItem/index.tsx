import { Checkbox } from 'antd';
import { history } from 'umi';
import styles from './index.less';
import { useEffect } from 'react';
import type { OrderItemProps } from './type';
import { getTime } from '@/utils/time';

export default function OrderItem(props: OrderItemProps) {
  //查看按钮 点击 跳转事件
  const onLook = () => {
    history.push(`/order/detail/${props.orderInfo.id}`);
    // console.log('look');
  };
  // useEffect(() => {
  //   console.log('打印传递子组件的参数', props);
  // });
  return (
    <div key={props?.orderInfo.id}>
      <div className={styles.item}>
        <div className={styles.top}>
          <div className={styles.top_check}>
            <Checkbox />
          </div>
          <div
            style={{
              backgroundImage: `url(${props?.orderInfo.productCover})`,
              backgroundRepeat: 'no-repeat',
              backgroundSize: 'cover',
              width: '99px',
              height: '60px',
              marginRight: '10px',
            }}
          >
            <div />
          </div>
          <div className={styles.top_name}>{props?.orderInfo.productName}</div>
          <div className={styles.top_price_old}>
            <p>￥{props.orderInfo.price}</p>
            <p className={styles.des}>原价</p>
          </div>
          <div className={styles.top_num}>
            <p>{props.orderInfo.count}</p>
            <p>购买数量</p>
          </div>
          <div className={styles.top_price_now}>
            <p>￥{props.orderInfo.price}</p>
            <p className={styles.des}>实付金额</p>
          </div>
          <div className={styles.top_status}>
            <button
              className={
                props.orderInfo.status === 0
                  ? styles.prepare
                  : props.orderInfo.status === 1
                  ? styles.cancel
                  : styles.delivery
              }
            >
              {props.orderInfo.status === 0
                ? '待付款'
                : props.orderInfo.status === 1
                ? '已取消'
                : '已完成'}
            </button>
          </div>
          <div className={styles.top_look} onClick={onLook}>
            查看
          </div>
        </div>
        <div className={styles.bottom}>
          <p>订单号：{props.orderInfo.id}</p>
          <p>下单时间：{getTime(props.orderInfo.createTime)}</p>
          <p>收货人：{props.orderInfo.receiverName}</p>
          <p>手机：{props.orderInfo.receiverPhone}</p>
          <p>取货门店：{props.orderInfo.receiverAddress}</p>
        </div>
      </div>
    </div>
  );
}
