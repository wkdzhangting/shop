import { Modal, Space, message } from 'antd';
import React, { useState } from 'react';
import { useModel } from 'umi';
import { history } from 'umi';
import styles from './index.less';
export type SiderTheme = 'light' | 'dark';
const GlobalHeaderRight: React.FC = () => {
  const [isModalOpen, setIsModalOpen] = useState(false);
  const { initialState } = useModel('@@initialState');
  if (!initialState || !initialState.settings) {
    return null;
  }
  const { navTheme, layout } = initialState.settings;
  let className = styles.right;
  if ((navTheme === 'dark' && layout === 'top') || layout === 'mix') {
    className = `${styles.right}  ${styles.dark}`;
  }

  // const showModal = () => {
  //   setIsModalOpen(true);
  // };

  const handleOk = () => {
    setIsModalOpen(false);
    localStorage.removeItem('token');
    history.push('/');
    message.open({
      type: 'success',
      content: '退出成功',
    });
  };

  const handleCancel = () => {
    setIsModalOpen(false);
  };

  const logOut = () => {
    setIsModalOpen(true);
    // console.log('退出');
  };

  return (
    <Space className={className}>
      <div style={{ display: 'flex', alignItems: 'center' }} onClick={logOut}>
        <img src={require('@/assets/out.png')} alt="" className={styles.outImg} />
        <button className={styles.out}>退出</button>
      </div>
      <div>
        <Modal title="提示" open={isModalOpen} onOk={handleOk} onCancel={handleCancel}>
          <p>您确认退出？</p>
        </Modal>
      </div>
    </Space>
  );
};
export default GlobalHeaderRight;
