import { PageContainer } from '@ant-design/pro-components';
import styles from './index.less';
import { Button, Form, Input, message } from 'antd';
import { useEffect, useState } from 'react';
import { modifyPassword, Request } from '../../services/ant-design-pro/user';
import type { FormProps } from 'antd';
import { history } from 'umi';

const Order = () => {
  //表单
  const [form] = Form.useForm();

  //提示框
  const [messageApi, contextHolder] = message.useMessage();

  const info = (e: string) => {
    messageApi.info(e);
  };

  //输入框绑定
  const [nowPwd, setNowPwd] = useState('');
  const [newPwd, setNewPwd] = useState('');
  const [rePwd, setRePwd] = useState('');

  const fetchModifyPassword = async (value: Request) => {
    console.log(value);

    const res = await modifyPassword(value);
    if (res.code === 0) {
      info('修改成功');
      // history.push('/welcome');
      localStorage.removeItem('token');
      history.push('/');
    } else {
      info(res.msg);

      // history.push('/welcome');
    }
  };

  const onFinish: FormProps<Request>['onFinish'] = (values) => {
    // console.log('Success:', values);
    if (newPwd === '' || nowPwd === '' || rePwd === '') {
      info('输入框不可为空');
      return;
    } else if (newPwd != rePwd) {
      info('新密码不一致');
      return;
    } else {
      fetchModifyPassword(values);
    }
  };

  return (
    <PageContainer>
      {contextHolder}

      <div className={styles.content}>
        <Form
          name="basic"
          form={form}
          labelCol={{ span: 8 }}
          wrapperCol={{ span: 16 }}
          style={{ maxWidth: 600 }}
          initialValues={{ remember: true }}
          autoComplete="off"
          onFinish={onFinish}
        >
          <Form.Item<Request>
            label="原密码"
            name="oldPassword"
            rules={[{ required: true, message: '请输入原密码' }]}
          >
            <Input.Password
              className={styles.passwordInput}
              value={nowPwd}
              onChange={(event) => setNowPwd(event.target.value)}
            />
          </Form.Item>

          <Form.Item<Request>
            label="新密码"
            name="newPassword"
            rules={[{ required: true, message: '请输入新密码!' }]}
          >
            <Input.Password
              className={styles.passwordInput}
              value={newPwd}
              onChange={(event) => setNewPwd(event.target.value)}
            />
          </Form.Item>

          <Form.Item<Request>
            label="确认新密码"
            name="repeatPassword"
            rules={[{ required: true, message: '请确认新密码' }]}
          >
            <Input.Password
              className={styles.passwordInput}
              value={rePwd}
              onChange={(event) => setRePwd(event.target.value)}
            />
          </Form.Item>

          <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
            <Button type="primary" htmlType="submit">
              确认修改
            </Button>
          </Form.Item>
        </Form>
      </div>
    </PageContainer>
  );
};
export default Order;
