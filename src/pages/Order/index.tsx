import { PageContainer } from '@ant-design/pro-components';
import styles from './index.less';
import { Button, Form, Input, Select } from 'antd';
import { useEffect, useState } from 'react';
import OrderItem from '@/components/OrderItem';
import type { OrderItemParams, getOrderListParams } from './type';
import { getOrderByCondition } from '../../services/ant-design-pro/order';

const Goods = () => {
  // const [orderStatus, setOrderStatus] = useState<number | null>(null);
  // const [orderId, setOrderId] = useState(0);
  // const [orderConsignee, serOrderConsignee] = useState('');
  //表单
  const [form] = Form.useForm();
  const [orderList, setOrderList] = useState<OrderItemParams[]>([]);
  //是否显示正在加载
  const [isLoading, setIsLoading] = useState(false);
  //是否有错误
  const [isError, setIsError] = useState(false);
  //查询参数
  const [searchParams, setSearchParams] = useState<getOrderListParams>({
    delete: false, //是否被删除
    orderId: null, //订单号，可以不传
    receiverName: '', //收件人，可以不传
    status: null, //查询状态，不传为全部信息，传了为对应状态：0待支付，1已取消，2已完成
  });
  //封装查询函数
  const fetchGetOrderByCondition = async (value: getOrderListParams) => {
    const res = await getOrderByCondition(value);
    if (res.code === 0) {
      //更新视图
      setOrderList(res.data.orderList);
      setSearchParams({ ...searchParams, delete: false });
      // console.log('打印搜索参数', searchParams);
      // console.log('打印搜索结果', res);
    } else {
      console.log('搜索失败', res);
    }
  };
  // //选择框 选择事件
  // const handleChange = (value: string) => {
  //   if (value === '全部') {
  //     setOrderStatus(null);
  //     setSearchParams({ ...searchParams, status: null });
  //   } else if (value === '待支付') {
  //     setOrderStatus(0);
  //     setSearchParams({ ...searchParams, status: 0 });
  //   } else if (value === '已取消') {
  //     setOrderStatus(1);
  //     setSearchParams({ ...searchParams, status: 1 });
  //   } else if (value === '已完成') {
  //     setOrderStatus(2);
  //     setSearchParams({ ...searchParams, status: 2 });
  //   } else if (value === '已删除') {
  //     setOrderStatus(null);
  //     setSearchParams({ ...searchParams, status: null, delete: true });
  //   } else {
  //     setOrderStatus(null);
  //     setSearchParams({ ...searchParams, status: null });
  //   }

  //   //  fetchGetOrderByCondition(searchParams);
  //   // console.log(`selected ${value}`);
  // };
  //查询事件
  const onFinish = (values: getOrderListParams) => {
    // 处理表单提交
    //fetchGetOrderByCondition(values)
    let orderId = form.getFieldValue('orderId');
    let orderConsignee = form.getFieldValue('orderConsignee');
    let orderStatus = form.getFieldValue('orderStatus');
    if (orderId === undefined || orderId === '') {
      orderId = null;
    }
    if (orderConsignee === undefined || orderId === '') {
      orderConsignee = '';
    }
    if (orderStatus === undefined || orderId === '') {
      orderStatus = null;
    }
    fetchGetOrderByCondition({
      orderId: orderId,
      receiverName: orderConsignee,
      status: orderStatus,
      delete: false,
    });
    console.log('打印搜索id', orderId, '打印搜索名', orderConsignee, '打印搜索状态', orderStatus);
  };

  //nav栏 button点击事件
  const getAll = () => {
    fetchGetOrderByCondition({ delete: false, orderId: null, receiverName: '', status: null });
  };

  const getWait = () => {
    fetchGetOrderByCondition({ delete: false, orderId: null, receiverName: '', status: 0 });
  };

  const getDel = () => {
    fetchGetOrderByCondition({ delete: true, orderId: null, receiverName: '', status: null });
  };

  useEffect(() => {
    setIsError(false);
    setIsLoading(true);
    try {
      //进页面获取全部订单信息
      fetchGetOrderByCondition(searchParams);
      console.log('获取的列表', orderList);
    } catch (e) {}
    setIsLoading(false);
  }, []);

  return (
    <PageContainer>
      <div className={styles.header}>
        <Form form={form} name="horizontal_login" layout="inline" onFinish={onFinish}>
          <Form.Item label="订单号:" name="orderId">
            <Input placeholder="请输入订单号数字" type="number" />
          </Form.Item>
          <Form.Item
            label="收货人:"
            name="orderConsignee"
            rules={[{ pattern: /^[A-Za-z\u4e00-\u9fa5\s-]+$/, message: '请输入有效的收货人姓名' }]}
          >
            <Input placeholder="请输入收货人姓名" />
          </Form.Item>
          <Form.Item label="订单状态:" name="orderStatus">
            <Select
              style={{ width: 120 }}
              defaultValue="请选择"
              options={[
                { value: null, label: '全部' },
                { value: '0', label: '待支付' },
                { value: '1', label: '已取消' },
                { value: '2', label: '已完成' },
              ]}
            />
          </Form.Item>
          <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
            <Button type="primary" htmlType="submit">
              搜索
            </Button>
          </Form.Item>
        </Form>
      </div>
      <div className={styles.nav}>
        <Button style={{ marginLeft: '0px' }} onClick={getAll}>
          全部
        </Button>
        <Button onClick={getWait}>待支付</Button>
        <Button>待发货</Button>
        <Button>待核销</Button>
        <Button onClick={getDel}>已删除</Button>
      </div>
      <div className={styles.content}>
        {isError && <div>出错了...</div>}
        {isLoading ? (
          <div>Loading ...</div>
        ) : (
          <div style={{ marginTop: '20px' }}>
            {orderList &&
              orderList.map((orderInfo: OrderItemParams) => (
                <OrderItem key={orderInfo.id} orderInfo={orderInfo} />
              ))}
          </div>
        )}
      </div>
    </PageContainer>
  );
};
export default Goods;
