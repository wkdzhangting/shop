export type OrderItemParams = {
  count: number;
  createTime: string;
  id: number;
  infoId: number;
  isDelete: boolean;
  payMethod: string;
  payTime: string;
  price: number;
  productCover: string;
  productId: number;
  productName: string;
  receiverAddress: string;
  receiverName: string;
  receiverPhone: string;
  status: number;
  updateTime: string;
  userId: number;
};

export type getOrderListParams = {
  delete: boolean;
  orderId: number | null;
  receiverName: string;
  status: number | null;
};
