// import { PageContainer } from '@ant-design/pro-components';
// import { Alert, Card, Typography } from 'antd';
// import React from 'react';
// import styles from './Welcome.less';
// const CodePreview: React.FC = ({ children }) => (
//   <pre className={styles.pre}>
//     <code>
//       <Typography.Text copyable>{children}</Typography.Text>
//     </code>
//   </pre>
// );
// const Welcome: React.FC = () => {
//   return (
//     <PageContainer>
//       <Card>
//         <Alert
//           message={'更快更强的重型组件，已经发布。'}
//           type="success"
//           showIcon
//           banner
//           style={{
//             margin: -12,
//             marginBottom: 24,
//           }}
//         />
//         <Typography.Text strong>
//           <a
//             href="https://procomponents.ant.design/components/table"
//             rel="noopener noreferrer"
//             target="__blank"
//           >
//             欢迎使用
//           </a>
//         </Typography.Text>
//         <CodePreview>yarn add @ant-design/pro-components</CodePreview>
//       </Card>
//     </PageContainer>
//   );
// };
// export default Welcome;

import { PageContainer } from '@ant-design/pro-components';
import { Typography } from 'antd';
import React, { useEffect, useState } from 'react';
import styles from './Welcome.less';
import {
  AlertOutlined,
  CloseCircleOutlined,
  ExceptionOutlined,
  InboxOutlined,
  MedicineBoxOutlined,
} from '@ant-design/icons';
import { getShow, Data } from '../services/ant-design-pro/welcome';
import { set } from 'rc-util';

// //订单详情内容
// let homeInfo: Data;

const Welcome: React.FC = () => {
  //是否显示正在加载
  const [homeInfo, setHomeInfo] = useState([]);
  //获取订单详情
  const fetchHomeInfo = async () => {
    try {
      const msg = await getShow();
      // homeInfo = msg.data;
      console.log(msg.data);
      setHomeInfo(msg.data);
      return msg.data;
    } catch (error) {
      console.log(error);
    }
    return undefined;
  };
  // const intl = useIntl();
  useEffect(() => {
    fetchHomeInfo();

    // setIsError(true);
  }, []);

  return (
    <div>
      <h1 className={styles.h1}>订单信息统计</h1>
      <div className={styles.content}>
        <div className={styles.item}>
          <div className={styles.imgItem}>
            <div className={styles.msgImg}>
              <MedicineBoxOutlined
                style={{ color: 'white', fontSize: '3.5rem' }}
                className={styles.logo}
              />
            </div>
          </div>
          <div className={styles.left}>
            <p className={styles.title}>待发货订单数</p>
            <div>
              <p className={styles.num}>{homeInfo?.unPayOrderNum}</p>个
            </div>
          </div>
        </div>
        <div className={styles.item}>
          <div className={styles.imgItem}>
            <div className={styles.orderImg}>
              <ExceptionOutlined
                style={{ color: 'white', fontSize: '3.5rem' }}
                className={styles.logo}
              />
            </div>
          </div>
          <div className={styles.left}>
            <p className={styles.title}>已成交订单数</p>
            <div>
              <p className={styles.num}>{homeInfo?.payOrderNum}</p>个
            </div>
          </div>
        </div>
        <div className={styles.item}>
          <div className={styles.imgItem}>
            <div className={styles.allImg}>
              <InboxOutlined
                style={{ color: 'white', fontSize: '3.5rem' }}
                className={styles.logo}
              />
            </div>
          </div>
          <div className={styles.left}>
            <p className={styles.title}>商品总数</p>
            <div>
              <p className={styles.num}>{homeInfo?.productNum}</p>个
            </div>
          </div>
        </div>
        <div className={styles.item}>
          <div className={styles.imgItem}>
            <div className={styles.nowImg}>
              <AlertOutlined
                style={{ color: 'white', fontSize: '3.5rem' }}
                className={styles.logo}
              />
            </div>
          </div>
          <div className={styles.left}>
            <p className={styles.title}>库存告警商品数</p>
            <div>
              <p className={styles.num}>{homeInfo?.productWarnNum}</p>个
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Welcome;
