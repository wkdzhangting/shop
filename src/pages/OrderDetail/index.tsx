import { PageContainer } from '@ant-design/pro-components';
import React, { useEffect } from 'react';
import styles from './index.less';
import { getOrderDetail } from '@/services/ant-design-pro/order';
import {
  InboxOutlined,
  MacCommandOutlined,
  PayCircleOutlined,
  WalletOutlined,
} from '@ant-design/icons';
import { useParams } from 'umi';
import { getTime } from '@/utils/time';

interface OrderDetailParams {
  id: string;
  productName: string;
  price: number;
  count: number;
  unit: string;
  weight: number;
  createTime: string;
  updateTime: string;
  payMethod: string;
  receiverName: string;
}
//订单详情内容
interface RouteParams {
  id?: string; // 将路由参数类型设置为 string | undefined
}
const Detail: React.FC = () => {
  const { id } = useParams<RouteParams>(); // 使用 useParams 钩子获取路由参数
  const [orderDetailInfo, setOrderDetailInfo] = React.useState<OrderDetailParams>({
    id: '',
    productName: '',
    price: 0,
    count: 0,
    unit: '',
    weight: 1,
    createTime: '',
    updateTime: '',
    payMethod: '',
    receiverName: '',
  });
  //获取详情内容
  const fetchDetailedInfo = async (value: string) => {
    const res = await getOrderDetail({ orderId: value });
    setOrderDetailInfo(res.data.orderDetail);
    console.log('根据订单编号获取到的订单详情', orderDetailInfo);
  };
  useEffect(() => {
    //获取详情内容
    fetchDetailedInfo(id as string);
    if (id) {
      console.log('传递的订单编号', id);
    }
  }, [id]);

  useEffect(() => {
    if (orderDetailInfo) {
      console.log('获取的订单详情', orderDetailInfo);
    }
  }, [orderDetailInfo]);

  return (
    <PageContainer>
      <div className={styles.top}>
        <h3>订单编号：{orderDetailInfo?.id}</h3>
        <div className={styles.top_msg}>
          <div className={styles.top_msg_startTime}>
            <p>下单时间：{getTime(orderDetailInfo.createTime)}</p>
          </div>
          <div className={styles.top_msg_payTime}>
            <p>付款时间：{getTime(orderDetailInfo?.updateTime)}</p>
          </div>
          <div className={styles.top_msg_client}>
            <p>购买用户：{orderDetailInfo?.receiverName}</p>
          </div>
          <div className={styles.top_msg_status}>
            <p>订单状态：</p>
            <button>已发货</button>
          </div>
          <div className={styles.top_msg_way}>
            <p>支付方式：{orderDetailInfo?.payMethod}</p>
          </div>
        </div>
        <div className={styles.top_dec}>
          <div className={styles.top_dec_title}>
            <p className={styles.title_1}>商品名称</p>
            <p className={styles.title_2}>货号</p>
            <p className={styles.title_2}>价格</p>
            <p className={styles.title_3}>数量</p>
            <p className={styles.title_3}>规格</p>
            <p className={styles.title_3}>库存</p>
            <p className={styles.title_3}>重量</p>
            <p className={styles.title_2}>小计</p>
          </div>
          <div className={styles.top_dec_msg}>
            <div className={styles.title_1}>{orderDetailInfo?.productName}</div>
            <div className={styles.title_2}>{orderDetailInfo?.id}</div>
            <div className={styles.title_2}>¥{orderDetailInfo?.price / orderDetailInfo?.count}</div>
            <div className={styles.title_3}>{orderDetailInfo?.count}</div>
            <div className={styles.title_3}>{orderDetailInfo?.unit}</div>
            <div className={styles.title_3}>1</div>
            <div className={styles.title_3}>
              {orderDetailInfo?.weight}
              kg
            </div>
            <div className={styles.title_2}>¥{orderDetailInfo?.price}</div>
          </div>
        </div>
      </div>
      <div className={styles.bottom}>
        <div className={styles.bottom_t}>
          <p>费用信息：</p>
        </div>
        <div className={styles.bottom_c}>
          <div className={styles.bottom_c_item}>
            <div className={styles.item_left}>
              <p>商品总金额：</p>
              <p>¥{orderDetailInfo?.price}</p>
            </div>
            <div className={styles.item_right}>
              <PayCircleOutlined
                className={styles.item_logo}
                style={{ color: 'white', fontSize: '4rem' }}
              />
            </div>
          </div>
          <div className={styles.bottom_c_item} style={{ backgroundColor: '#F6C291' }}>
            <div className={styles.item_left}>
              <p>配送费用：</p>
              <p>¥0.00</p>
            </div>
            <div className={styles.item_right}>
              <InboxOutlined
                className={styles.item_logo}
                style={{ color: 'white', fontSize: '4rem' }}
              />
            </div>
          </div>
          <div className={styles.bottom_c_item} style={{ backgroundColor: '#A5D899' }}>
            <div className={styles.item_left}>
              <p>已付款金额：</p>
              <p>¥0.00</p>
            </div>
            <div className={styles.item_right}>
              <WalletOutlined
                className={styles.item_logo}
                style={{ color: 'white', fontSize: '4rem' }}
              />
            </div>
          </div>
          <div className={styles.bottom_c_item} style={{ backgroundColor: '#81CBE1' }}>
            <div className={styles.item_left}>
              <p>使用优惠卷：</p>
              <p>¥0.00</p>
            </div>
            <div className={styles.item_right}>
              <MacCommandOutlined
                className={styles.item_logo}
                style={{ color: 'white', fontSize: '4rem' }}
              />
            </div>
          </div>
        </div>
        <div className={styles.bottom_b}>
          <p>
            已付款金额：
            <span> ¥{orderDetailInfo?.price}</span>
          </p>
        </div>
      </div>
    </PageContainer>
  );
};

export default Detail;
