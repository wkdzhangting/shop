import { Button, Form, Input, Select } from 'antd';
import styles from './index.less';
import { useEffect } from 'react';

export type formDataType = {
  name: string;
  channelId: number | undefined;
};

export type CategoryType = { value: number; label: string };

type FormType = {
  categoryList: CategoryType[];
  onSearchFormChange: (formData: formDataType) => void;
};

const SearchForm: React.FC<FormType> = (props) => {
  const [form] = Form.useForm();

  const handleSearchGoodsList = (values: formDataType) => {
    props.onSearchFormChange(values);
    // form.resetFields();
  };

  const onReset = () => {
    // console.log('这是子组件的重置表单数据', values);
    props.onSearchFormChange({
      name: '',
      channelId: undefined,
    });
    form.resetFields();
  };

  return (
    <>
      <Form
        style={{ minWidth: '850px' }}
        onFinish={handleSearchGoodsList}
        form={form}
        name="horizontal_login"
        layout="inline"
      >
        <Form.Item label="商品名称:" name="name">
          <Input placeholder="请输入关键字" style={{ width: 290 }} />
        </Form.Item>
        <Form.Item label="商品分类:" name="channelId">
          <Select style={{ width: 144 }} options={props.categoryList} />
        </Form.Item>
        <Form.Item>
          <Button className={styles.searchBtn} type="primary" htmlType="submit">
            搜索
          </Button>
          <Button className={styles.searchBtn} htmlType="button" onClick={onReset} type="primary">
            Reset
          </Button>
        </Form.Item>
      </Form>
    </>
  );
};
export default SearchForm;
