import type { TableColumnsType } from 'antd';
import { Button, Pagination, Table, message } from 'antd';
import type { DataType } from '../../index';

import styles from './index.less';
import React, { useState } from 'react';
import { delGoodList } from '@/services/ant-design-pro/good';

type pageType = {
  total: number;
  pageSize: number;
  current: number;
  pageSizeOptions: number[];
  onChange: (page: number, pageSize: number) => void;
};

type TableType = {
  data: DataType[];
  columns: TableColumnsType<DataType>;
  total?: number;
  pageSize?: number;
  currentPage?: number;
  totalPage?: number;
  pageSizeOptions?: number[];
  pagination?: pageType;
  onTableChange?: (pageNumber: number, pageSize: number) => void;
  onPageChange?: (page: number) => void;
  onReset?: () => void;
  onInit?: () => void;
};

const Tables: React.FC<TableType> = (props) => {
  const [selectedRowKeys, setSelectedRowKeys] = useState<React.Key[]>([]);
  const [messageApi, contextHolder] = message.useMessage();

  const deleteGoodsList = async () => {
    if (selectedRowKeys.length == 0) {
      messageApi.warning('请选择需要操作的数据');
      return;
    }
    try {
      const ids = selectedRowKeys.join(',');
      const res = await delGoodList(ids);
      if (res.code === 0) {
        messageApi.success('批量删除成功');
        setSelectedRowKeys([]);
        props?.onInit();
      } else {
        messageApi.error(res.msg);
      }
    } catch (error) {
      console.log('批量删除过程中出现错误');
    }
    //删除成功后需要提示批量删除成功并且重新刷新表格数据
    // if (res.code == 1) {
    //   messageApi.open({
    //     type: 'success',
    //     content: 'This is a success message',
    //   });
    // }
  };

  const rowSelection = {
    onChange: (newSelectedRowKeys: React.Key[]) => {
      console.log('selectedRowKeys changed: ', newSelectedRowKeys);
      setSelectedRowKeys(newSelectedRowKeys);
    },
  };
  return (
    <>
      {contextHolder}
      <div className={styles.title}>
        <Button type="primary" danger onClick={deleteGoodsList}>
          批量删除
        </Button>
      </div>
      <Table
        columns={props.columns}
        dataSource={props.data.map((item) => ({
          ...item,
          key: item.id,
        }))}
        // pagination={false}
        scroll={{ x: 'max-content' }}
        size="middle"
        rowSelection={{
          type: 'checkbox',
          ...rowSelection,
        }}
        pagination={props.pagination}
      />

      {/* <div className={styles.bottom}>
        <div className={styles.word}>
          共 {props.total} 条记录 第{props.currentPage}/{props.totalPage}页
        </div>
        <Pagination
          showQuickJumper
          showSizeChanger
          current={props.currentPage} //当前页数
          pageSize={props.pageSize} //分页大小
          total={props.total} //数据总数
          onChange={props?.onTableChange} // 页码变化重新请求数据
          className="pagination"
          pageSizeOptions={props.pageSizeOptions}
        />
      </div> */}
    </>
  );
};

export default Tables;
