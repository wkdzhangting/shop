import { request } from 'umi';

export interface categoryParams {
  channelLogo: string;
  channelName: string;
  description: string;
  ordered: number;
  status: number;
  [property: string]: any;
}

//删除分类
export async function delCategory(id: number) {
  return request(`/admin/api/v1/channel/delete?id=${id}`, {
    method: 'DELETE',
  });
}
export interface Data {
  orderId: number;
  [property: string]: any;
}
// export interface addCategoryParams {
//   channelLogo: string;
//   channelName: string;
//   description: string;
//   ordered: number;
//   status: number;
//   [property: string]: any;
// }

//新增分类
export async function addCategory(params: categoryParams) {
  return request('/admin/api/v1/channel/add', {
    method: 'POST',
    data: params,
  });
}

export interface ModifyCategoryParams extends categoryParams {
  id: number;
}
//修改分类
export async function modifyCategory(params: ModifyCategoryParams) {
  return request('/admin/api/v1/channel/update', {
    method: 'PUT',
    data: params,
  });
}

export async function getSingleCategoryInfo(id: number) {
  return request(`/admin/api/v1/channel/${id}`, {
    method: 'GET',
  });
}
