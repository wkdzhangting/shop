import { request } from 'umi';

export interface Data {
  payOrderNum: number;
  productNum: number;
  productWarnNum: number;
  unPayOrderNum: number;
  [property: string]: any;
}

export async function getShow() {
  return request(`/admin/api/v1/show`, {
    method: 'GET',
  });
}
