import { request } from 'umi';

/**
 * Request
 */
export interface Request {
  newPassword: string;
  oldPassword: string;
  repeatPassword: string;
  [property: string]: any;
}
export async function modifyPassword(params: Request) {
  return request('/admin/api/v1/update', {
    method: 'PUT',
    data: params,
  });
}
