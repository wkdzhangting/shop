import { request } from 'umi';

export interface Query_Type_GetGoodsListParams {
  channelId?: number | undefined;
  name?: string;
  page?: number;
  pageSize?: number;
  [property: string]: any;
}

/**
 * 获取商品列表(7.1)
 * @param params getGoodsListParams
 * @returns
 */
export async function getGoodsList(params?: Query_Type_GetGoodsListParams) {
  return request('/admin/api/v1/product/page', {
    method: 'GET',
    params,
  });
}

// export interface getSingleGoodParams {
//   id: number;
// }
/**
 * 获取单个商品信息(3.4)
 * @param params getSingleGoodParams
 * @returns
 */
export async function getSingleGoodInfo(id: string) {
  return request(`/admin/api/v1/product/${id}`, {
    method: 'GET',
  });
}

// export interface delGoodParams {
//   goodsId: number;
// }
// /**
//  * 删除商品(3.2)
//  * @param params getSingleGoodParams
//  * @returns
//  */
// export async function delGood(params: delGoodParams) {
//   return request('/admin/goods', {
//     method: 'DELETE',
//     data: params,
//   });
// }

/**
 * 管理员批量删除商品
 */
export interface Request {
  ids: string[];
  [property: string]: any;
}

export async function delGoodList(ids: string) {
  return request(`/admin/api/v1/product/deleteByIds?ids=${ids}`, {
    method: 'DELETE',
    // data: params,
  });
}

export interface GoodParams {
  channelId: number;
  content: string;
  coverUrl: string;
  description: string;
  imgs: string[];
  name: string;
  note: string;
  // ordered: string;
  price: number;
  stocks: number;
  unit: string;
  warnStocks: number;
  weight: number;
  [property: string]: any;
}
/**
 * 新增商品(3.1)
 * @param params GoodParams
 * @returns
 */
export async function addGood(params: GoodParams) {
  return request('/admin/api/v1/product/add', {
    method: 'POST',
    data: params,
  });
}

export interface ModifyGoodParams extends GoodParams {
  id: number;
}

/**
 * 修改商品(3.3)
 * @param params ModifyGoodParams
 * @returns
 */
export async function modifyGood(params: ModifyGoodParams) {
  return request('/admin/api/v1/product/updateWithInfo', {
    method: 'PUT',
    data: params,
  });
}

/**
 * 管理员修改商品状态、排序、虚拟销量
 */
export interface changeGoodStatusParams {
  id: number;
  ordered?: number;
  status?: number;
  virtualSale?: number;
  [property: string]: any;
}
export async function changeGoodStatus(params: changeGoodStatusParams) {
  return request('/admin/api/v1/product/update', {
    method: 'PUT',
    data: params,
  });
}

/**
 * 管理端上传图片
 */

export const uploadPicture = (params: any) => {
  return request('/admin/api/v1/common/upload', {
    method: 'POST',
    data: params,
  });
};

/**
 * 管理员查询分类
 */

export const getCategoryList = () => {
  return request('/admin/api/v1/channel/getAllChannel');
};

/**
 * 管理员修改商品状态、排序、虚拟销量
 */
export interface changeGoodPartInfoParams {
  id: number;
  ordered?: number;
  status?: number;
  virtualSale?: number;
  [property: string]: any;
}

export const changeGoodPartInfo = (params: changeGoodPartInfoParams) => {
  return request('/admin/api/v1/product/update', {
    method: 'PUT',
    data: params,
  });
};

export interface loginParams {
  account: string;
  passWord: string;
  [property: string]: any;
}
export const loginAdmin = (params: loginParams) => {
  return request('/admin/api/v1/login', {
    method: 'POST',
    data: params,
  });
};
