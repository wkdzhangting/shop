import type { getOrderListParams } from '@/pages/Order/type';
import { request } from 'umi';

/**
 * 获取订单详情
 */

/**
 * 获取商品列表(7.1)
 * @param params getGoodsListParams
 * @returns
 */
export async function getOrderDetail(params) {
  return request('/admin/api/v1/order/getOrderById', {
    method: 'GET',
    params,
  });
}

export async function getOrderByCondition(params: getOrderListParams) {
  return request('/admin/api/v1/order/getOrderByCondition', {
    method: 'POST',
    data: params,
  });
}

/**
 * 返回管理员主界面返回信息概括
 */
export async function getChartInfo() {
  return request('/admin/api/v1/show', {
    method: 'GET',
  });
}
