/**--no-ignore */
import { Request, Response } from 'umi';

export default {
  /**
   *
   * @param req 批量删除商品
   * @param res
   */
  'DELETE /admin/api/v1/product/deleteByIds': (req: Request, res: Response) => {
    res.status(200).send({
      code: 0,
      msg: '操作成功',
      data: {},
    });
  },
  'PUT /admin/api/v1/product/updateWithInfo': (req: Request, res: Response) => {
    res.status(200).send({
      code: 0,
      msg: '操作成功',
      data: null,
    });
  },
  /**
   *
   * @param req
   * @param res
   */
  'PUT /admin/api/v1/product/update': (req: Request, res: Response) => {
    res.status(200).send({
      code: 0,
      msg: '操作成功',
      data: null,
    });
  },
  'GET /admin/api/v1/product/page': (req: Request, res: Response) => {
    const page = parseInt(req.query.page || '1', 10);
    const pageSize = 5;
    const totalRecords = 8;

    const totalPages = Math.ceil(totalRecords / pageSize);
    const startIdx = (page - 1) * pageSize;
    const endIdx = Math.min(startIdx + pageSize, totalRecords);

    const records = [];

    // 模拟数据
    for (let i = startIdx + 1; i <= endIdx; i++) {
      records.push({
        id: i,
        name: '茅台dsvdefevfevfdeavvefavefdavf  fgb',
        price: 1999.0,
        coverUrl:
          'https://haluo-store.oss-cn-wuhan-lr.aliyuncs.com/resource/avatar/4485554740750336/v1.1713490181913.jpg',
        status: 1,
        ordered: 100,
        virtualSale: 0,
      });
    }

    res.status(200).send({
      code: 0,
      msg: 'success',
      data: {
        records,
        total: totalRecords,
        size: pageSize,
        current: page,
        orders: [],
        optimizeCountSql: true,
        hitCount: false,
        countId: null,
        maxLimit: null,
        searchCount: true,
        pages: totalPages,
      },
    });
  },

  // 'GET /admin/api/v1/product/page': (req: Request, res: Response) => {
  //   res.status(200).send({
  //     code: 1,
  //     msg: 'success',
  //     data: {
  //       records: [
  //         {
  //           id: 14,
  //           name: '茅台',
  //           price: 1999.0,
  //           coverUrl:
  //             'https://haluo-store.oss-cn-wuhan-lr.aliyuncs.com/resource/avatar/4485554740750336/v1.1713490181913.jpg',
  //           status: 1,
  //           ordered: 100,
  //           virtualSale: 0,
  //         },
  //         {
  //           id: 12,
  //           name: '茅台',
  //           price: 1999.0,
  //           coverUrl:
  //             'https://haluo-store.oss-cn-wuhan-lr.aliyuncs.com/resource/avatar/4485554740750336/v1.1713490181913.jpg',
  //           status: 1,
  //           ordered: 100,
  //           virtualSale: 0,
  //         },
  //         {
  //           id: 32,
  //           name: '茅台',
  //           price: 1999.0,
  //           coverUrl:
  //             'https://haluo-store.oss-cn-wuhan-lr.aliyuncs.com/resource/avatar/4485554740750336/v1.1713490181913.jpg',
  //           status: 1,
  //           ordered: 100,
  //           virtualSale: 0,
  //         },
  //         {
  //           id: 24,
  //           name: '茅台',
  //           price: 1999.0,
  //           coverUrl:
  //             'https://haluo-store.oss-cn-wuhan-lr.aliyuncs.com/resource/avatar/4485554740750336/v1.1713490181913.jpg',
  //           status: 1,
  //           ordered: 100,
  //           virtualSale: 0,
  //         },
  //       ],
  //       total: 30,
  //       size: 5,
  //       current: 2,
  //       orders: [],
  //       optimizeCountSql: true,
  //       hitCount: false,
  //       countId: null,
  //       maxLimit: null,
  //       searchCount: true,
  //       pages: 5,
  //     },
  //   });
  // },
  'DELETE /admin/goods': (req: Request, res: Response) => {
    let { goodsId } = req.body;
    res.status(200).send({
      code: goodsId,
      msg: '删除商品成功',
      data: {},
    });
  },
  'GET /admin/api/v1/product/:id': (req: Request, res: Response) => {
    res.status(200).send({
      code: 0,
      msg: '操作成功',
      data: {
        id: 1,
        name: '苹果',
        channelId: 1,
        channelName: '水果生鲜',
        description: '山东红富士苹果 贱卖',
        price: 10.98,
        coverUrl:
          'https://haluo-store.oss-cn-wuhan-lr.aliyuncs.com/resource/avatar/4485554740750336/v1.1713490181913.jpg',
        imgs: [
          'https://haluo-store.oss-cn-wuhan-lr.aliyuncs.com/resource/avatar/4485554740750336/v1.1713490181913.jpg',
          'https://haluo-store.oss-cn-wuhan-lr.aliyuncs.com/resource/avatar/4485554740750336/v1.1713490181913.jpg',
        ],
        weight: 3.0,
        unit: 'kg',
        stocks: 100,
        warnStocks: 20,
        note: '山东一手货源',
        content:
          '<h1>"<strong>thtyhth5th45h45h56h5h5r45hg455t4h45h"</strong></h1><ol><li>cui</li><li>zhang</li><li>yang</li></ol><h1><strong>我爱你</strong></h1>',
        isOver: false,
        status: 1,
        ordered: 1,
        virtualSale: 1,
      },
    });
  },
  'POST /admin/api/v1/product/add': (req: Request, res: Response) => {
    res.status(200).send({
      code: 0,
      msg: '操作成功',
      data: null,
    });
  },
  'GET /api/v1/channel/getAllChannel': (req: Request, res: Response) => {
    res.status(200).send({
      code: 0,
      msg: '操作成功',
      data: [
        {
          id: 1,
          channelName: '水果生鲜',
          channelLogo:
            'https://haluo-store.oss-cn-wuhan-lr.aliyuncs.com/default/%E9%BB%98%E8%AE%A4%E5%88%86%E7%B1%BBlogo.png',
          status: 1,
          ordered: 100,
          description: null,
        },
        {
          id: 2,
          channelName: '食品粮油',
          channelLogo:
            'https://haluo-store.oss-cn-wuhan-lr.aliyuncs.com/default/%E9%BB%98%E8%AE%A4%E5%88%86%E7%B1%BBlogo.png',
          status: 1,
          ordered: 101,
          description: null,
        },
        {
          id: 3,
          channelName: '特卖促销',
          channelLogo:
            'https://haluo-store.oss-cn-wuhan-lr.aliyuncs.com/default/%E9%BB%98%E8%AE%A4%E5%88%86%E7%B1%BBlogo.png',
          status: 0,
          ordered: 102,
          description: null,
        },
        {
          id: 4,
          channelName: '进口水果',
          channelLogo:
            'https://haluo-store.oss-cn-wuhan-lr.aliyuncs.com/default/%E9%BB%98%E8%AE%A4%E5%88%86%E7%B1%BBlogo.png',
          status: 0,
          ordered: 103,
          description: null,
        },
        {
          id: 5,
          channelName: '有机食品',
          channelLogo:
            'https://haluo-store.oss-cn-wuhan-lr.aliyuncs.com/default/%E9%BB%98%E8%AE%A4%E5%88%86%E7%B1%BBlogo.png',
          status: 0,
          ordered: 104,
          description: null,
        },
        {
          id: 6,
          channelName: '特色美食',
          channelLogo:
            'https://haluo-store.oss-cn-wuhan-lr.aliyuncs.com/default/%E9%BB%98%E8%AE%A4%E5%88%86%E7%B1%BBlogo.png',
          status: 0,
          ordered: 105,
          description: null,
        },
        {
          id: 7,
          channelName: '肉类主食',
          channelLogo:
            'https://haluo-store.oss-cn-wuhan-lr.aliyuncs.com/default/%E9%BB%98%E8%AE%A4%E5%88%86%E7%B1%BBlogo.png',
          status: 0,
          ordered: 106,
          description: null,
        },
        {
          id: 8,
          channelName: '乳制零食',
          channelLogo:
            'https://haluo-store.oss-cn-wuhan-lr.aliyuncs.com/default/%E9%BB%98%E8%AE%A4%E5%88%86%E7%B1%BBlogo.png',
          status: 0,
          ordered: 107,
          description: null,
        },
        {
          id: 16,
          channelName: '手机111',
          channelLogo: '1.png',
          status: 0,
          ordered: 100,
          description: null,
        },
      ],
    });
  },
  'POST  /admin/api/v1/login': (req: Request, res: Response) => {
    const { username, password } = req.body;
    console.log('111');

    // 检查用户名和密码
    if (username === 'admin' && password === 'admin') {
      // 返回 token
      res.status(200).json({
        code: 0,
        msg: 'success',
        data: {
          token:
            'eyJhbGciOiJIUzUxMiIsInppcCI6IkdaSVAifQ.H4sIAAAAAAAAADWLSwoDIRAF79LrEbTt8TO3saOCgYBEJQkhd0_PYnibVxT1hftscIDeI3PAqkzOpChWq6LnqhzeMORcNLsIG7Q04TDeWIfoXNxgLJZ6fMYsj9OPIfhagk_BtLJg6l1-efcrlUnaxBGFHS2hRkPoUf_-wJ33sZAAAAA.Kg3H1rEpZfJsNm84OQPkpw5FDpEB92KP4d5-jumtLD-cEC8E_g8XB_CPyi_pMJlg2mEWQkqv5QCu5IENLn8cCw',
        },
      });
    } else if (username !== 'admin') {
      // 用户名错误
      res.status(200).json({
        code: 1,
        msg: '用户不存在',
      });
    } else {
      // 密码错误
      res.status(200).json({
        code: 2,
        msg: '密码错误',
      });
    }
  },
};
