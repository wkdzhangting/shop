/**--no-ignore */
import { Request, Response } from 'umi';

export default {
  /**
   *
   * @param req 删除分类
   * @param res
   */
  'DELETE /admin/api/v1/channel/delete': (req: Request, res: Response) => {
    res.status(200).send({
      code: 0,
      msg: '操作成功',
      data: {},
    });
  },
  /**
   *
   * @param 更新分类
   * @param res
   */
  'PUT /admin/api/v1/channel/update': (req: Request, res: Response) => {
    res.status(200).send({
      code: 0,
      msg: '操作成功',
      data: null,
    });
  },

  // 添加分类
  'POST /admin/api/v1/channel/add': (req: Request, res: Response) => {
    res.status(200).send({
      code: 0,
      msg: '操作成功',
      data: null,
    });
  },

  // 获取分类信息
  'GET /admin/api/v1/channel/:id': (req: Request, res: Response) => {
    res.status(200).send({
      code: 0,
      msg: '操作成功',
      data: {
        id: 1,
        channelName: '水果生鲜',
        channelLogo:
          'https://haluo-store.oss-cn-wuhan-lr.aliyuncs.com/default/%E9%BB%98%E8%AE%A4%E5%88%86%E7%B1%BBlogo.png',
        status: 0,
        ordered: 100,
        description: '1',
      },
    });
  },
};
