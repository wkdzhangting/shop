/**--no-ignore */
import { Request, Response } from 'umi';

export default {
  /**
   * 管理端上传图片通用接口
   * @param req
   * @param res
   */
  'POST /admin/api/v1/common/upload': (req: Request, res: Response) => {
    res.status(200).send({
      code: 0,
      msg: '操作成功',
      data: {
        url: 'https://haluo-store.oss-cn-wuhan-lr.aliyuncs.com/resource/channel/4485234202142720/v1.1713489790441.png',
      },
    });
  },
};
