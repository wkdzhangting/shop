import { Request, Response } from 'umi';

export default {
  // 获取分类信息
  'GET /admin/api/v1/show': (req: Request, res: Response) => {
    res.status(200).send({
      code: 0,
      msg: '操作成功',
      data: {
        unPayOrderNum: 0,
        payOrderNum: 1,
        productNum: 7,
        productWarnNum: 0,
      },
    });
  },
};
