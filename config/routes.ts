import { layout } from '@/app';
import route from 'mock/route';
import { Redirect } from 'umi';

export default [
  {
    path: '/user',
    layout: false,
    routes: [
      { name: '登录', path: '/user/login', component: './user/Login' },
      { component: './404' },
    ],
  },

  { path: '/welcome', name: '欢迎', icon: 'smile', component: './Welcome' },
  {
    path: '/admin',
    name: '商品', // 修改名称为商品管理
    icon: 'crown',
    access: 'canAdmin',
    Redirect: '/admin/good-page',
    // component: './Good',
    routes: [
      { path: '/admin/good-page', name: '商品管理', icon: 'smile', component: './Good' },
      { path: '/admin/category-page', name: '分类管理', icon: 'smile', component: './Category' },
      // { component: './404' }, // 404页面
    ],
  },
  {
    path: '/order',
    name: '订单',
    icon: 'crown',
    access: 'canAdmin',
    routes: [
      {
        path: '/order',
        name: '订单列表',
        icon: 'smile',
        component: './Order',
      },
      {
        path: '/order/detail/:id',
        name: '订单详情',
        component: './OrderDetail',
        hideInMenu: true, // 不显示在菜单栏中
      },
    ],
  },
  {
    path: '/person',
    name: '个人',
    icon: 'crown',
    routes: [
      { path: '/person/person-pwd', name: '修改密码', icon: 'smile', component: './Person' },
    ],
  },
  { path: '/', redirect: './welcome' },
];
